<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class articleController extends Controller
{
    public function create(Request $request, $id = null)
    {
        // param Validation
        $this->validate($request, [
            'title' => 'required|min:20',
            'content' => 'required|min:200',
            'category' => 'required|min:3',
            'status' => 'required|in:Publish,Draft,Thrash',

        ]);

        if (!$id) {
            try {
                DB::table('posts')->insert([
                    'title' => $request->title,
                    'content' => $request->content,
                    'category' => $request->category,
                    'status' => $request->status
                ]);
                return response(["message"   => 'Success Create Article'], 201);
            } catch (\Throwable $th) {
                return $th->getMessage();
            }
        } else {
            try {
                $check = DB::table('posts')->find($id);
                if ($check==null) {
                    return response(["message"   => 'Article id Not Found'], 404);
                }

                $affected = DB::table('posts')
                ->where('id', $id)
                ->update([
                    'title' => $request->title,
                    'content' => $request->content,
                    'category' => $request->category,
                    'status' => $request->status
                ]);
                return response(["message"   => 'Success Update Article id '. $id], 200);
            } catch (\Throwable $th) {
                return $th->getMessage();
            }

        }
    }

    public function getList(Request $request,$limit=0,$offset=0){
        $check = DB::table('posts')->select('id','title','content','category','status')->offset($offset)->limit($limit)->get();
        return response($check, 200);
    }

    public function getDetail(Request $request,$id=null){
        $check = DB::table('posts')->select('id','title','content','category','status')->where('id',$id)->get();
        if (count($check)==0) {
            return response(["message"   => 'Article id Not Found'], 404);
        }
        return response($check, 200);
    }
    public function delete(Request $request,$id=null){
        $check = DB::table('posts')->find($id);
        if ($check==null) {
            return response(["message"   => 'Article id Not Found'], 404);
        }

        try {
            $deleted = DB::table('posts')->where('id',$id)->delete();
            return response(["message"   => 'Success delete Article id '. $id], 404);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }


    }
}
