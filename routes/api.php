<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([
    'prefix' => '/api',
], function () use ($router) {
    $router->post('/article/{id}', 'api\v1\articleController@create');
    $router->post('/article', 'api\v1\articleController@create');
    $router->get('/article/{limit}/{offset}', 'api\v1\articleController@getList');
    $router->get('/article/{id}', 'api\v1\articleController@getDetail');
    $router->delete('/article/{id}', 'api\v1\articleController@delete');




    // $router->post('/report', 'v1\bimaController@report');
});
